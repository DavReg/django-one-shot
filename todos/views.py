from django.shortcuts import render
from todos.models import TodoList

# Create your views here.


def todo_list_list(request):
    List = TodoList.objects.all()
    context = {
        "list": list,
    }
    return render(request, "todo_lists/list.html", context)
